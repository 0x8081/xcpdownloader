using System;
using System.Net;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Ionic.Zlib;
using RestSharp;
using XboxWebApi.Extensions;
using XboxWebApi.Services;
using XboxWebApi.Services.Api;
using XboxWebApi.Services.Model.X360Marketplace;

namespace xcpDownloader
{
    class Program
    {
        readonly static int PAGE_SIZE = 25;
        readonly static XblLocale LANGUAGE = XblLanguage.United_States;

        static long index = 0;
        const int CHUNK = 1024;
        const int HEADER_SIZE = (44 * 1024);
        const int OPT_BUFF_SIZE = (16 * 1024);
        const int DATA_SIZE = 170459136;

        static string directory;

        enum CONTENT_TYPES{
            ARCADE_TITLE    = 0xD0000,
            AVATAR_ITEM     = 0x9000,
            GAME_DEMO       = 0x80000,
            XBOX_DOWNLOAD   = 0x7000
        };

        static void Main(string[] args) {
            if (args.Length > 0) {
                string input = args[0];
                var client = new RestClient();
                var overview = GetOverview(client);
                XmlDocument item = GetItem(client, Guid.Parse(args[0]));

                XmlNodeList titleID = item.GetElementsByTagName("hexTitleId");
                XmlNodeList contentID = item.GetElementsByTagName("contentId");
                byte[] convertBytes = Convert.FromBase64String(contentID[0].InnerXml);
                string contentIDHex = BitConverter.ToString(convertBytes).Replace("-", string.Empty);
                directory = contentIDHex + ".data";
                string xcp = contentIDHex + ".xcp";
                string xup = contentIDHex + ".xup";

                if (File.Exists(xcp)) {
                    Console.WriteLine(xcp + " already exists!");
                }
                else {
                    string URL = "http://download.xboxlive.com/content/" + titleID[0].InnerXml.Replace("0x", string.Empty) + "/" + contentIDHex.ToLower() + ".xcp";
                    Console.WriteLine("Downloading:" + URL);
                    WebClient webClient = new WebClient();
                    webClient.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler(DownloadComplete);
                    webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(Progress);
                    webClient.DownloadFileAsync(new Uri(URL), xcp);
                    while (webClient.IsBusy) {};
                }
                DoShit(xcp, xup);
                Console.WriteLine("Finished!");
            }
            else {
                Console.WriteLine("Usage: xcpDownloader.exe <ProductID>");
            }
            return;
        }

        static void Progress(object sender, DownloadProgressChangedEventArgs e) {
            Console.WriteLine((e.BytesReceived / 1024) + "KB / " + (e.TotalBytesToReceive / 1024) + "KB " + e.ProgressPercentage + "%");
        }

        static void DownloadComplete(object sender, EventArgs e) {
            Console.WriteLine("Download Completed.");
        }

        public static XmlDocument LoadXml(string content) {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(content);
            return xml;
        }

        public static XmlDocument GetOverview(IRestClient client, int start = 0) {
            var query = new XboxWebApi.Services.Model.X360Marketplace
                .CatalogOverviewRequestQuery(XblLanguage.United_States);
            query.DetailView = CatalogDetailLevel.Minimal;
            query.PageSize = PAGE_SIZE;
            query.PageNum = 1 + (start / PAGE_SIZE);

            client.BaseUrl = new Uri("http://marketplace-xb.xboxlive.com");
            RestRequestEx request = new RestRequestEx(
                $"Catalog/Catalog.asmx/Query", Method.GET);

            List<Tuple<string, string>> q = query.GetQuery();
            // Query contains duplicate keys, hence following add-method
            q.ForEach(x => request.AddQueryParameter(x.Item1, x.Item2));

            IRestResponse response = client.Execute(request);
            return LoadXml(response.Content);
        }

        public static XmlDocument GetItem(IRestClient client, Guid productId) {
            CatalogItemRequestQuery query = new CatalogItemRequestQuery();

            RestRequestEx request = new RestRequestEx(
                $"marketplacecatalog/v1/product/{LANGUAGE.Locale}/{productId}", Method.GET);
            request.AddQueryParameters(query.GetQuery());

            IRestResponse<CatalogItemResponse> response = client
                .Execute<CatalogItemResponse>(request);
            return LoadXml(response.Content);
        }

        static void inflate(FileStream src, FileStream dest) {
            byte[] data = new byte[CHUNK];

            ZlibStream stream = new ZlibStream(src, CompressionMode.Decompress, true) {
                BufferSize = CHUNK,
                FlushMode = FlushType.Full
            };

            while (stream.Read(data, 0, data.Length) != 0) {
                dest.Write(data, 0, data.Length);
            }

            index += stream.TotalIn;
            src.Position = index;
            stream.Dispose();
        }

        static void strip(FileStream xup) {
            Console.WriteLine("Writting segments into SVOD Format");
            long size = xup.Length;
            long index = 0;

            byte[] header_buff = new byte[HEADER_SIZE + 1];
            xup.Read(header_buff, 0, HEADER_SIZE);
            FileStream header = File.Open(directory.Replace(".data", ""), FileMode.Create, FileAccess.Write);
            header.Write(header_buff, 0, HEADER_SIZE);
            index += HEADER_SIZE;


            if (!System.IO.Directory.Exists(directory)) {
                System.IO.Directory.CreateDirectory(directory);
            }

            int data_segment = 0;
            byte[] data_buff = new byte[OPT_BUFF_SIZE];
            while (index < size) {
                string fname = string.Format("{0}\\Data{1:D4}", directory, data_segment);
                FileStream data = File.Open(fname, FileMode.Create, FileAccess.Write);
                for (int i = 0; i < (DATA_SIZE / OPT_BUFF_SIZE); i++) {
                    if (index + OPT_BUFF_SIZE < size) {
                        xup.Read(data_buff, 0, OPT_BUFF_SIZE);
                        data.Write(data_buff, 0, OPT_BUFF_SIZE);
                        index += OPT_BUFF_SIZE;
                    }
                    else {
                        xup.Read(data_buff, 0, (int)(size - index));
                        data.Write(data_buff, 0, (int)(size - index));
                        index += (size - index);
                    }
                }
                data_segment++;
            }
            Console.WriteLine("Finished writting out to SVOD Format");
        }

        static void DoShit(string fin, string fout) {
            FileStream xcp = File.Open(fin, FileMode.Open, FileAccess.Read);
            FileStream xup = File.Open(fout, FileMode.Create, FileAccess.ReadWrite);

            long size = xcp.Length;

            Console.WriteLine("Decompressing...");
            while ((size - index) > 0) {
                inflate(xcp, xup);
            }

            xup.Seek(0x344, SeekOrigin.Begin);
            byte[] content_type = new byte[4];
            xup.Read(content_type, 0, 4);
            Array.Reverse(content_type);
            int val = BitConverter.ToInt32(content_type, 0);
            if (val == (int)CONTENT_TYPES.XBOX_DOWNLOAD) {
                xup.Seek(0, SeekOrigin.Begin);
                strip(xup);
            }
            else {
                Console.WriteLine("Content type is not Game on Demand, skipping");
            }
        }
    }
}
